package br.com.alura.client;

import com.google.gson.Gson;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;

public class ClientHttpConfiguration {

  public HttpResponse<String> dispararRequisicaoPost(String uri, Object object)
      throws IOException, InterruptedException {
    HttpClient client = HttpClient.newHttpClient();
    HttpRequest request = HttpRequest.newBuilder().uri(URI.create(uri)).header("Content-Type", "application/json")
        .method("POST", BodyPublishers.ofString(new Gson().toJson(object))).build();
    return client.send(request, BodyHandlers.ofString());
  }

  public HttpResponse<String> dispararRequisicaoGet(String uri)
      throws IOException, InterruptedException {
    HttpClient client = HttpClient.newHttpClient();
    HttpRequest request = HttpRequest.newBuilder().uri(URI.create(uri)).method("GET", BodyPublishers.noBody()).build();
    return client.send(request, BodyHandlers.ofString());
  }
}
